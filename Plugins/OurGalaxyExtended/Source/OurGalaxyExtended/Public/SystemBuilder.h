// Gradess & Novaturion Company. All rights reserved.

// ReSharper disable CppClassCanBeFinal
#pragma once

DECLARE_LOG_CATEGORY_EXTERN(LogSystemBuilder, Display, All);

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SystemBuilder.generated.h"

UCLASS(Blueprintable)
class OURGALAXYEXTENDED_API ASystemBuilder : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    ASystemBuilder();

protected:

    /**
    * Assigns a new scene name (label) to this actor.  Actor labels are only available in development builds.
    * @param    TargetActor    The actor whose name will be changed
    * @param	Label    The new label string to assign to the actor.  If empty, the actor will have a default label.
    * @param	bMarkDirty    If true the actor's package will be marked dirty for saving.  Otherwise it will not be.  You should pass false for this parameter if dirtying is not allowed (like during loads)
    */
    UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "SystemBuilder", meta=(AdvancedDisplay=2))
    void SetActorSceneName(
        UPARAM(DisplayName="Actor") AActor* TargetActor,
        UPARAM(DisplayName="New Label") const FString& Label,
        UPARAM(DisplayName="Mark Dirty") const bool bMarkDirty = true
    );
};
